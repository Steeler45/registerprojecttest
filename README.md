# RegisterProjectTest

# Technologies used :
	1.Spring Boot 1.5.4.RELEASE
	2.Angular 5.2.0
	3.PostgreSQL 10
	4.PrimeNG 5.2.4
	5.Java 8
	6.Gradle

# Source Code :
	Back end : RegisterProject
	Front end : RegisterFrontend

# Quick start application :
	1.Create database "reg" (PostgreSQL).
	2.Clone git https://Steeler45@bitbucket.org/Steeler45/registerprojecttest.git
	3.Install jdk1.8 and create JAVA_HOME
	4.Double click runReg.bat
	5.Open URL: http://localhost:8080 in your browser.

# Manual setup :
	1.Create database "reg" (PostgreSQL).
	2.Clone git https://Steeler45@bitbucket.org/Steeler45/registerprojecttest.git
	3.Import project RegisterProject to IDE. (Existing gradle project)
	4.Open cmd and go to RegisterProject directory. (start spring boot project)
		gradlew runBoot
	5.Open cmd and go to RegisterFrontend directory. (start angular project)
		npm install
		npm start
	6.Open URL: http://localhost:4200 in your browser

# Database Config :
	PostgreSQL
	host = localhost
	port = 5432
	username = postgres
	password = 1234

	**You can change at RegisterProject/src/main/resources/application.properties

