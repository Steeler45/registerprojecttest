package com.pj.reg.controller;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pj.reg.bean.ResponseBean;
import com.pj.reg.entity.User;
import com.pj.reg.service.UserService;

@RestController
@RequestMapping("/api")
public class RegisterController {

	@Autowired
	UserService userService;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	
	@PostMapping("/register")
	public ResponseEntity<ResponseBean> add(@RequestParam("fileImage") MultipartFile file
			,@RequestParam("user") String jsonString) {

		ObjectMapper mapper = new ObjectMapper();
		try {
			User user = mapper.readValue(jsonString, User.class);
			
			User userDup = userService.getUserByUsername(user.getUsername());
			
			if(userDup != null) {
				return new ResponseEntity<ResponseBean>(ResponseBean.ResponseWarning("Username already exists."), HttpStatus.OK);
			}else {
				userService.save(user,file);
				return new ResponseEntity<ResponseBean>(ResponseBean.ResponseSuccessfully(), HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			return new ResponseEntity<ResponseBean>(ResponseBean.ResponseError(e), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping("/getUsers")
	public List<User> getUsers() {
		try {
			List<User> users = userService.finddAll();
			users.forEach(user -> {
				if(user.getProfileImage() != null) {
					String urlDownload = MvcUriComponentsBuilder.fromMethodName(RegisterController.class, "getFileImageProfile", user.getProfileImage()).build().toString();
					user.setProfileImage(urlDownload);
				}
			});
			return users;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			return Collections.emptyList();
		}
		
	}
	
	@PostMapping("/getUserLogin")
	public User getUserLogin() {
		try {
			User user = userService.getUserByUserLogin();
			if(user.getProfileImage() != null) {
				String urlDownload = MvcUriComponentsBuilder.fromMethodName(RegisterController.class, "getFileImageProfile", user.getProfileImage()).build().toString();
				user.setProfileImage(urlDownload);
			}
			return user;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			return null;
		}
	}
	
	@GetMapping("/files/profile/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> getFileImageProfile(@PathVariable String filename) {
		Resource file = userService.loadFileUserProfile(filename);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}
	
}
