package com.pj.reg.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pj.reg.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);

}
