package com.pj.reg.service.impl;


import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.pj.reg.entity.User;
import com.pj.reg.repository.UserRepository;
import com.pj.reg.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	private Path rootLocation = Paths.get("upload-dir");

	@Override
	public void save(User user, MultipartFile file) throws IOException {
		// TODO Auto-generated method stub

		String[] fileNameWithExtension = file.getOriginalFilename().split("\\.");
		String extension = fileNameWithExtension.length>1?fileNameWithExtension[fileNameWithExtension.length-1]:null;
		
		String filenameGenerate = user.getUsername()+"."+extension;
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setProfileImage(filenameGenerate);
		
		userRepository.save(user);

		//store file
		if(!rootLocation.toFile().isDirectory()) {
			Files.createDirectory(rootLocation);
		}
		Files.copy(file.getInputStream(), this.rootLocation.resolve(filenameGenerate));
	}

	@Override
	public List<User> finddAll() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}

	@Override
	public Resource loadFileUserProfile(String filename) {
		this.rootLocation = Paths.get("upload-dir");
		try {
			Path file = rootLocation.resolve(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new RuntimeException("FAIL!");
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("FAIL!");
		}
	}

	@Override
	public User getUserByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public User getUserByUserLogin() {
		// TODO Auto-generated method stub
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return getUserByUsername(auth.getName());
	}

	
	
}
