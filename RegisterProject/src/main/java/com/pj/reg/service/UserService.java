package com.pj.reg.service;

import java.io.IOException;
import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import com.pj.reg.entity.User;

public interface UserService{
	
	void save(User user, MultipartFile file) throws IOException;
	
	List<User> finddAll();
	
	User getUserByUsername(String username);
	
	User getUserByUserLogin();
	
	Resource loadFileUserProfile(String filename);
	
}
