package com.pj.reg.service.impl;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.pj.reg.entity.User;
import com.pj.reg.repository.UserRepository;

import static java.util.Collections.emptyList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
		private UserRepository userRepository;

		public UserDetailsServiceImpl(UserRepository userRepository) {
			this.userRepository = userRepository;
		}

		@Override
		public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
			User user = userRepository.findByUsername(username);
			if (user == null) {
				throw new UsernameNotFoundException(username);
			}
			return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), emptyList());
		}
}
