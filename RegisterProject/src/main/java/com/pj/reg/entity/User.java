package com.pj.reg.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="register")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "register_generator")
	@SequenceGenerator(name="register_generator", sequenceName = "register_seq", allocationSize=1)
	@Column(name = "register_id", updatable = false, nullable = false)
	private Long registerId;
	
	@Column
	private String email;
	
	@Column
	private String username;
	
	@Column
	private String password;
	
	@Column(name="profile_image")
	private String profileImage;
	
	@Transient
	private String confirmPassword;

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(Long registerId, String email, String username, String password, String profileImage,
			String confirmPassword) {
		super();
		this.registerId = registerId;
		this.email = email;
		this.username = username;
		this.password = password;
		this.profileImage = profileImage;
		this.confirmPassword = confirmPassword;
	}

	public Long getRegisterId() {
		return registerId;
	}

	public void setRegisterId(Long registerId) {
		this.registerId = registerId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

}
