package com.pj.reg.bean;

public class ResponseBean {

	private Boolean status;
	private Message massage;
	
	public ResponseBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ResponseBean(Boolean status, Message massage) {
		super();
		this.status = status;
		this.massage = massage;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Message getMassage() {
		return massage;
	}
	public void setMassage(Message massage) {
		this.massage = massage;
	}
	

	public static ResponseBean ResponseSuccessfully() {
		return new ResponseBean(true, new Message("success", "Success Message", "Successfully"));
	}
	public static ResponseBean ResponseError(Exception e) {
		return new ResponseBean(false,new Message("error", "Error Message", e.getMessage()));
	}
	public static ResponseBean ResponseError(String msg) {
		return new ResponseBean(false,new Message("error", "Error Message", msg));
	}
	public static ResponseBean ResponseWarning(String msg) {
		return new ResponseBean(false, new Message("warn", "Warn Message", msg));
	}
	public static ResponseBean ResponseSuccessfully(String str) {
		return new ResponseBean(true, new Message("success", "Success Message", str));
	}
	
}
