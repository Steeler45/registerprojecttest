import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, AbstractControl, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Message } from 'primeng/components/common/message';
import { passwordConfirming } from '../common/ValidateCustom';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  msgs: Message[] = [];
  emailFilter: RegExp = /[0-9a-zA-Z@._\-]+$/;

  fileImage:any = null;
  formData: FormData;

  constructor(private fb: FormBuilder, 
    private router: Router,
    private httpClient: HttpClient) { }

  ngOnInit() {
    this.signupForm = this.fb.group({
      'email': new FormControl('', [Validators.pattern("[a-zA-Z0-9-_.]*@[^ @]*.[^ @]")]),
      'username': new FormControl('', [Validators.required, Validators.maxLength(12)]),
      'password': new FormControl('', Validators.required),
      'confirmPassword': new FormControl('', [Validators.required, passwordConfirming])
    });
  }

  signup(value){
    this.formData = new FormData();
    this.formData.append("user",JSON.stringify(value));
    this.formData.append("fileImage",this.fileImage);
    
    this.httpClient.post("/api/register",this.formData).subscribe(
      res => {
        this.msgs = [];
        if(res['status']){
          this.router.navigate(['/login']);
        }else{
          this.msgs.push(res['massage']);
        }
      },
      error => {
        this.msgs = [];
        this.msgs.push({severity:'error', summary:'Error Message', detail:error});
      }
    );
  }

  onSelectImage($event){
    this.fileImage = $event.files[0];
    
  }

}
