import { FormControl } from "@angular/forms";

export function passwordConfirming(control: FormControl) { 
    if(!control.parent || !control) return null;
    const pwd = control.parent.get('password');
    const cpwd= control.parent.get('confirmPassword')

    if(!pwd || !cpwd) return null;
    return (pwd.value !== cpwd.value)? { nomatch: true } :null;
}