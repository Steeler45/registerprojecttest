import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthHttp, JwtHelper } from 'angular2-jwt';
import { HttpClient } from '@angular/common/http';
import { RequestOptions, Headers, Http, Response } from '@angular/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  users: any = [];
  userLogin:any = null;
  constructor(public router: Router, private http: Http) {}

  ngOnInit() {


let headers = new Headers({ 'Content-Type': 'application/json','Authorization' : localStorage.getItem('jwt')});
let options = new RequestOptions({ headers: headers });

    setTimeout(() => {
      this.http.post('/api/getUserLogin',{}, options).subscribe(
        res => {
          this.userLogin = res.json();
        },
        errorr => {}
      );
      this.http.post('/api/getUsers', {}, options).subscribe(
        res => {
          this.users = res.json();
        }
      );
    }, 600);
    
  }

  logout() {
    localStorage.removeItem('jwt');
    this.router.navigate(['login']);
  }

  extractData(res: Response) {
    let body = res.json();
    return body || {};
}

handleErrorPromise(error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
}

}
