import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpEvent, HttpEventType } from '@angular/common/http';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Message } from 'primeng/components/common/message';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  msgs: Message[] = [];
  constructor(private fb: FormBuilder,
    public router: Router,
  private loginService: LoginService) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      'username': new FormControl('', Validators.required),
      'password': new FormControl('', Validators.required)
    });
  }

  login(values) {
    this.loginService.login(values).subscribe(res => {
      this.router.navigate(['home']);
    },
    error=>{
      
      this.msgs = [];
      if(error.status == "401"){
        this.msgs.push({severity:'error', summary:'Error Message', detail:"Check your username and password."});
      }
    
    });
  }


}
