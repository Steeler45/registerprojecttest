import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { ReplaySubject, Observable } from "rxjs";
import { Http } from '@angular/http';

@Injectable()
export class LoginService {

  constructor(private readonly http: Http) { }

  authUser = new ReplaySubject<any>(1);
  login(values: any): Observable<any> {
    return this.http.post('/api/login', values)
      .map(response => this.handleJwtResponse(response.headers.get('authorization')));
  }

  private handleJwtResponse(jwt: string) {
    localStorage.setItem('jwt', jwt);
    this.authUser.next(jwt);
    return jwt
  }
  
}
